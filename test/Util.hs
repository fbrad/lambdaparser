module Util where

import Syntax.Expression
import Syntax.Grammar (parseProgram)
import qualified Data.Map as M

internal :: String -> Expression
internal = maybe (error "Syntax error!") head . parseProgram

makeContext :: [(String, String)] -> Context
makeContext = M.fromList . map (fmap internal)