module Evaluation.Normal where

import Syntax.Expression
import Evaluation.Substitution
import Control.Monad.State
import qualified Data.Map as M

{-|
    Small-step normal-order evaluation of a given expression,
    within a given context.
-}
eval :: Expression             -- ^ Expression to be evaluated
     -> Context                -- ^ Context where the evaluation takes place
     -> (Expression, Context)  -- ^ Evaluation result, together with a possibly
                               --   enriched context, in case of definition
evalM :: Expression
	-> Eval Expression


evalM e@(Var x) = do
	--OLD VERSION: state $ \ctx -> (expand e ctx, ctx)
	ctx <- get
	return (expand e ctx)

evalM (Definition lval rval) = do 
	result <- evalM rval
	modify (M.insert lval result)
	return result
	--OLD VERSION: state $ \ctx -> (result, M.insert lval result ctx)

evalM e@(Function x body) = return e
	--OLD VERSION: state $ \ctx -> (e, ctx)


{-
evalM (Appl bredex@(Appl (Function x body) e) rexp) = do
	ctx <- get
	return $ reduce (Appl (reduce bredex ctx) (expand rexp ctx)) ctx
	--OLD VERSION: 	state $ \ctx -> let r = reduce (Appl (reduce bredex ctx) (expand rexp ctx)) ctx in 
	--									(r, ctx)
-}

evalM app@(Appl (Function x body) e2) = return $ subst x e2 body

evalM (Appl e1 e2) = do
	e <- evalM e1
	return $ Appl e e2

--evalM (Appl e1 e2) = liftM2 $ Appl (evalM e1) (return e2)

{-
evalM e@(Appl lexp rexp) = do
	ctx <- get
	if expandable lexp ctx then 
		--state $ \ctx -> ((Appl (expand lexp ctx) rexp), ctx)
		return $ Appl (expand lexp ctx) rexp
	else 
		if expandable rexp ctx then
			--state $ \ctx -> ((Appl lexp (expand rexp ctx)), ctx)
			return $ Appl lexp (expand rexp ctx)
		else
			--state $ \ctx -> ((reduce e ctx), ctx)
			return $ reduce e ctx
-}

eval e ctx = runState (evalM e) ctx

{-
eval e ctx = case e of
	(Definition lval rval) -> ((fst result), M.insert lval (fst result) ctx) where 
									result = eval rval ctx
	(Var x) -> (expand e ctx, ctx)
	(Function x body) -> (e, ctx)
	--left expression is a beta-redex
	(Appl bredex@(Appl (Function x body) e2) rexp) -> (result, ctx) where
								result = reduce (Appl (reduce bredex ctx) (expand rexp ctx)) ctx
	(Appl lexp rexp) 
		| expandable lexp ctx -> ((Appl (expand lexp ctx) rexp), ctx)
		| expandable rexp ctx -> ((Appl lexp (expand rexp ctx)), ctx)
		| otherwise -> ((reduce e ctx), ctx)
-}
	
{-
	Given an expression, returns the binded expression if it appears inside
	the context or the expression itself otherwise.
-}
expand :: Expression -> Context -> Expression
expand e ctx = case e of
	(Var x) -> case (M.lookup x ctx) of
				Nothing -> e
				Just value -> value
	otherwise -> e

{-
	Returns True if a given expression is binded inside the context or False
	otherwise.
-}
expandable :: Expression -> Context -> Bool
expandable e ctx = case e of
	(Var x) -> case (M.lookup x ctx) of
				Nothing -> False
				Just value -> True
	otherwise -> False

{-z
	Performs beta-reduction.
-}
reduce :: Expression -> Context -> Expression
reduce e ctx = case e of
	(Appl e1@(Function x body) e2) -> subst x e2 body
	otherwise -> e

