module Evaluation.Applicative where

import Syntax.Expression
import Evaluation.Substitution
import qualified Data.Map as M

{-|
    Small-step applicative-order evaluation of a given expression,
    within a given context.
-}
eval :: Expression             -- ^ Expression to be evaluated
     -> Context                -- ^ Context where the evaluation takes place
     -> (Expression, Context)  -- ^ Evaluation result, together with a possibly
                               --   enriched context, in case of definition

reduce :: Expression -> Expression
reduce e = case e of
	(Appl (Function x e1) f@(Function y e2)) -> subst x f e1
	otherwise -> e

eval e ctx = case e of
	(Definition lval rval) -> ((fst result), M.insert lval (fst result) ctx) where 
									result = eval rval ctx
	(Appl lexp rexp) -> case lexp of
							-- beta-reduce lexp
							(Appl (Function x body) e2) -> (result, ctx) where
								result = reduce $ Appl (reduce lexp) rexp
							
							-- expand id
							(Var x) -> (result, ctx) where
								result = case (M.lookup x ctx) of
									Nothing -> e
									Just value -> (Appl value rexp)

							(Function x body) -> case rexp of
													(Appl e1 e2) -> (result, ctx) where
														result = reduce $ Appl lexp (reduce rexp)
													otherwise -> (reduce e, ctx)
													--TODO: maybe expand rexp

							otherwise -> (result, ctx) where
								result = reduce e
	(Var x) -> case (M.lookup x ctx) of 
		-- x = value
		Nothing -> (e, ctx)
		(Just value) -> (value, ctx)
	(Function x body) -> (e, ctx)
