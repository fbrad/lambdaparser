module Syntax.Grammar where

import Syntax.Expression
import Syntax.Parser
import Control.Applicative


parseProgram :: String -> Maybe [Expression]
parseProgram str = parse (some expression) str

whitespace :: Parser String
whitespace = many $ spot (`elem` " \n\r") 

expression :: Parser Expression
expression = (\w e -> e) <$> whitespace <*> (definition <|> application <|> function <|> variable)

variable :: Parser Expression
variable = Var <$> some letter 

function :: Parser Expression
function = (\tok1 var tok2 expr -> Function var expr) <$>
    (token '\\') <*> (some letter) <*> (token '.') <*> expression

application :: Parser Expression
application = (\tok1 expr1 tok2 expr2 tok3 -> Appl expr1 expr2) <$>
    (token '(') <*> expression <*> (token ' ') <*> expression <*> (token ')')

{-partial application
application = (\e1 e2 -> Appl e1 e2) <$>
	(token '(') *> expression <* (token ' ') <*> expression <* (token ')')
-}

definition :: Parser Expression
definition = (\str tok1 expr -> Definition str expr) <$>
    (some letter) <*> (token '=') <*> expression
