module Syntax.Expression where

import Control.Monad.State
import qualified Data.Map as M

data Expression = Var String |
          Function String Expression |
          Appl Expression Expression |
          Definition String Expression deriving (Eq)

type Context = M.Map String Expression
type Eval a = State Context a

instance Show Expression where
	show (Var x) = x
	show (Function x e) = "\\" ++ x ++ "." ++ (show e)
	show (Appl e1 e2) = "(" ++ (show e1) ++ " " ++ (show e2) ++ ")"
	show (Definition x def) = x ++ "=" ++ (show def)