module Evaluation.Substitution where

import Syntax.Expression
import Data.Set

{-|
    Returns the list of free variables in an expression.
-}
freeVars :: Expression -> [String]
freeVars e = elems $ freeVarsSet e

freeVarsSet :: Expression -> Set String
freeVarsSet e = case e of 
		(Var v) -> fromList [v]
		(Function str e) -> difference (freeVarsSet e) (fromList [str])
		(Appl e1 e2) -> union (freeVarsSet e1) (freeVarsSet e2)
		(Definition id e) -> freeVarsSet e


{-|
    Performs the substitution of the free occurrences of a variable within
    an expression with another expression.
-}
subst :: String      -- ^ Variable
      -> Expression  -- ^ New expression
      -> Expression  -- ^ Existing expression
      -> Expression  -- ^ Resulting expression

subst x e2 e1 = case e1 of
	(Var y) | y == x -> e2
			| otherwise -> e1
	(Function y e) | y == x -> e1
				   | notElem y (freeVars e2) -> Function y (subst x e2 e)
				   | otherwise -> Function (y ++ "#") (subst x e2 convertedExpr) where convertedExpr = (subst y (Var $ y ++ "#") e)
	(Appl eleft eright) -> (Appl (subst x e2 eleft) (subst x e2 eright))

		
