module Evaluation.Big where

import Syntax.Expression
import Evaluation.Normal

{-|
    Big-step evaluation of a given expression, within a given context.
    The evaluation should stop when either the value is reached,
    or the expression cannot be reduced further.
    
    The first argument is the small-step evaluation function.
-}
evalBig :: (Expression -> Context -> (Expression, Context))  -- ^ Small-stepper
        -> Expression             -- ^ Expression to be evaluated
        -> Context                -- ^ Context where the evaluation takes place
        -> (Expression, Context)  -- ^ Evaluation result,
                                  --   together with a possibly enriched context
                                  --   in case of definition
evalBig stepper e ctx 
  | e == evalE = (e, ctx)
  | otherwise = evalBig stepper evalE evalCtx
    where (evalE, evalCtx) = stepper e ctx 
                        
{-|
    Big-step evaluation of a list of expressions, starting with
    the given context and using it throughout the entire list,
    for propagating the encountered definitions.
    
    The first argument is the small-step evaluation function.
-}
evalList :: (Expression -> Context -> (Expression, Context))
         -> [Expression]
         -> Context
         -> ([Expression], Context)

evalList stepper eList ctx = evalList' stepper eList ctx [] 

evalList' stepper [] ctx accList = (accList, ctx)
evalList' stepper (e:restE) ctx accList = evalList' stepper restE newCtx (accList ++ [evalE]) where
                                            (evalE, newCtx) = evalBig stepper e ctx

