module Evaluation.Normal where

import Syntax.Expression
import Evaluation.Substitution
import qualified Data.Map as M

{-|
    Small-step normal-order evaluation of a given expression,
    within a given context.
-}
eval :: Expression             -- ^ Expression to be evaluated
     -> Context                -- ^ Context where the evaluation takes place
     -> (Expression, Context)  -- ^ Evaluation result, together with a possibly
                               --   enriched context, in case of definition

{-
	Given an expression, returns the binded expression if it appears inside
	the context or the expression itself otherwise.
-}
expand :: Expression -> Context -> Expression
expand e ctx = case e of
	(Var x) -> case (M.lookup x ctx) of
				Nothing -> e
				Just value -> value
	otherwise -> e

{-
	Returns True if a given expression is binded inside the context or False
	otherwise.
-}
expandable :: Expression -> Context -> Bool
expandable e ctx = case e of
	(Var x) -> case (M.lookup x ctx) of
				Nothing -> False
				Just value -> True
	otherwise -> False

{-
	Performs beta-reduction.
-}
reduce :: Expression -> Context -> Expression
reduce e ctx = case e of
	(Appl (Function x e) e2@(Var y)) -> subst x (expand e2 ctx) e
	(Appl (Function x e1) e2) -> subst x e2 e1
	otherwise -> e

eval e ctx = case e of
	(Definition lval rval) -> ((fst result), M.insert lval (fst result) ctx) where 
									result = eval rval ctx
	(Var x) -> (expand e ctx, ctx)
	(Function x body) -> (e, ctx)
	--left expression is a beta-redex
	(Appl bredex@(Appl (Function x body) e2) rexp) -> (result, ctx) where
								result = reduce (Appl (reduce bredex ctx) (expand rexp ctx)) ctx
	(Appl lexp rexp) 
		| expandable lexp ctx -> ((Appl (expand lexp ctx) rexp), ctx)
		| expandable rexp ctx -> ((Appl lexp (expand rexp ctx)), ctx)
		| otherwise -> ((reduce e ctx), ctx)
	
	